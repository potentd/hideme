import winreg
import os

def files_dir():
    if not os.path.exists("C:\\Windows\\Temp\\hideme"):
        os.system("mkdir C:\\Windows\\Temp\\hideme")
    else:
        pass

def vm_keys():
    # VMware
    vmware_key = winreg.CreateKey(winreg.HKEY_LOCAL_MACHINE,
                                   'SOFTWARE\\VMware, Inc.\\VMware Tools')
    winreg.SetValueEx(vmware_key, 'InstallPath', 0, winreg.REG_SZ,
                       'C:\\Program Files\\VMware\\VMware Tools')
    winreg.CloseKey(vmware_key)

    # Virtualbox
    virtualbox_key = winreg.CreateKey(winreg.HKEY_LOCAL_MACHINE,
                                       'SOFTWARE\\Oracle\\VirtualBox')
    winreg.SetValueEx(virtualbox_key, 'InstallDir', 0, winreg.REG_SZ,
                        'C:\\Program Files\\Oracle\\VirtualBox')
    winreg.CloseKey(virtualbox_key)

    # QEMU
    qemu_key = winreg.CreateKey(winreg.HKEY_LOCAL_MACHINE,
                                 'SOFTWARE\\QEMU')
    winreg.SetValueEx(qemu_key, 'InstallPath', 0, winreg.REG_SZ,
                       'C:\\Program Files\\qemu')
    winreg.CloseKey(qemu_key)

def vm_dll():
    dlls = [
        'C:\\Windows\\System32\\Drivers\\Vmmouse.sys',
        'C:\\Windows\\System32\\Drivers\\vm3dgl.dll',
        'C:\\Windows\\System32\\Drivers\\vmdum.dll',
        'C:\\Windows\\System32\\Drivers\\VmGuestLibJava.dll',
        'C:\\Windows\\System32\\Drivers\\vm3dver.dll',
        'C:\\Windows\\System32\\Drivers\\vmtray.dll',
        'C:\\Windows\\System32\\Drivers\\VMToolsHook.dll',
        'C:\\Windows\\System32\\Drivers\\vmGuestLib.dll',
        'C:\\Windows\\System32\\Drivers\\vmhgfs.dll',

				'C:\\Windows\\System32\\Drivers\\VBoxMouse.sys',
        'C:\\Windows\\System32\\Drivers\\VBoxGuest.sys',
        'C:\\Windows\\System32\\Drivers\\VBoxSF.sys',
        'C:\\Windows\\System32\\Drivers\\VBoxVideo.sys',
        'C:\\Windows\\System32\\vboxoglpackspu.dll',
        'C:\\Windows\\System32\\vboxoglpassthroughspu.dll',
        'C:\\Windows\\System32\\vboxservice.exe',
        'C:\\Windows\\System32\\vboxoglcrutil.dll',
        'C:\\Windows\\System32\\vboxdisp.dll',
        'C:\\Windows\\System32\\vboxhook.dll',
        'C:\\Windows\\System32\\vboxmrxnp.dll',
        'C:\\Windows\\System32\\vboxogl.dll',
        'C:\\Windows\\System32\\vboxtray.exe',
        'C:\\Windows\\System32\\VBoxControl.exe',
        'C:\\Windows\\System32\\vboxoglerrorspu.dll',
        'C:\\Windows\\System32\\vboxoglfeedbackspu.dll',
        'c:\\Windows\\system32\\vboxoglarrayspu.dll'
    ]
    for dll in dlls:
        file_path = os.path.join(dll)
        open(file_path, 'a').close()

def vm_processes():
    import subprocess
    files_dir_path = "C:\\Windows\\Temp\\hideme"
    process_names = [
        'x32dbg.exe', 'x96dbg.exe', 'x64dbg.exe', 'ida64.exe', 
        'vboxservice.exe', 'wireshark.exe', 'vmtoolsd.exe', 
        'vmwaretray.exe', 'vmwareuser.exe', 'fakenet.exe', 
        'vboxtray.exe', 'joeboxcontrol.exe', 'vmwaretrat.exe', 
        'vmacthlp.exe', 'vmware.exe'
    ]
    
    for process_name in process_names:
        process_path = os.path.join(files_dir_path, process_name)
        
        with open(process_path, 'w') as file:
            file.write('')

        subprocess.Popen(['cmd', '/c', 'start', '/b', process_path], creationflags=subprocess.CREATE_NEW_PROCESS_GROUP)