import os, subprocess

files_dir_path = "C:\\Windows\\Temp\\hideme"
process_names = [
        'x32dbg.py', 'x96dbg.py', 'x64dbg.py', 'ida64.py', 
        'vboxservice.py', 'wireshark.py', 'vmtoolsd.py', 
        'vmwaretray.py', 'vmwareuser.py', 'fakenet.py', 
        'vboxtray.py', 'joeboxcontrol.py', 'vmwaretrat.py', 
        'vmacthlp.py', 'vmware.py'
]
    
def create():
    
    for process_name in process_names:
        process_path = os.path.join(files_dir_path, process_name)
        
        with open(process_path, 'w') as file:
            file.write('')
            
def convert():
	for file_name in os.listdir(files_dir_path):
		if file_name.endswith('.py'):
			file_path = os.path.join(files_dir_path, file_name)
			exe_name = file_name.replace('.py', '')
			exe_path = os.path.join(files_dir_path, exe_name + '.exe')
			subprocess.run(['pyinstaller', '--onefile', file_path, '--distpath', files_dir_path, '--noconsole', '--name', exe_name])

create()
convert()