import os
import time
from vms import *

logo = R'''  _     _     _                     
 | |   (_)   | |                    
 | |__  _  __| | ___ _ __ ___   ___ 
 | '_ \| |/ _` |/ _ \ '_ ` _ \ / _ \
 | | | | | (_| |  __/ | | | | |  __/
 |_| |_|_|\__,_|\___|_| |_| |_|\___|
  
 hideme - become the vm
'''

def print_logo():
		print(logo)
                                    
def menu():
		print_logo()
		print(f"1. setup\n0. exit\n")
  
		choice = input(">> ")

		if choice == "1":
			setup()
		elif choice == "0":
			exit()
		else:
			os.system("clear || cls")
			menu()

def setup():
		os.system("clear || cls")
		print_logo()
		print(f"1. default\n2. advanced\n0. exit\n")
	
		choice = input(">> ")

		if choice == "1":
			defaultSetup()
		elif choice == "2":
			advancedSetup()
  
def defaultSetup():
		print("changing registry keys...")
		vm_keys()
		time.sleep(1)
		print("creating dlls and drivers...")
		vm_dll()
		time.sleep(1)
		print("creating fake processes and services...")
		vm_processes()
		time.sleep(1)
		print("finished all. restart your computer to see changes.")
  
def advancedSetup():
		print("you chose advanced setup")