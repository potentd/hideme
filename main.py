from menu import *
import ctypes
import sys

def is_admin():
    try:
        return ctypes.windll.shell32.IsUserAnAdmin() == 1
    except:
        return False

if is_admin():
    files_dir()
    menu()
else:
    print("please run hideme as administrator")
    input("press enter to exit...")
    sys.exit(1)
